package mn.ticket.ticket.repository;

import mn.ticket.ticket.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
