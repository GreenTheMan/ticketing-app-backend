package mn.ticket.ticket.repository;

import mn.ticket.ticket.dto.EventDto;
import mn.ticket.ticket.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    @Query("SELECT new mn.ticket.ticket.dto.EventDto(e.id, e.name) FROM Event e" +
            "WHERE (:name = null OR e.name LIKE '%:name')" +
            "AND (:description = null OR e.description LIKE '%:description%')" +
            "AND (:category = null OR c.id = :categoryId)" +
            "LEFT JOIN Category c ON e.category_id=c.id")
    List<EventDto> findAllByNameAndDescriptionAndCategory(String name, String description, Long categoryId);

}
