package mn.ticket.ticket.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import mn.ticket.ticket.dto.EventDto;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.print.Doc;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name="event", schema = "ticketschema")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @Column(name = "date", nullable = false)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private LocalDateTime date;

    @Column(name = "description")
    private String description;

    @Column(name = "location_lat", nullable = false)
    private Double locationLat;

    @Column(name = "location_long", nullable = false)
    private Double locationLong;

    @OneToMany(mappedBy = "event")
    private List<Documentation> documentations;

    @OneToOne(mappedBy = "event")
    private Ticket ticket;

}
