package mn.ticket.ticket.model;

public enum PayoutMethod {
    CARD, CASH, SOCIALPAY;
}
