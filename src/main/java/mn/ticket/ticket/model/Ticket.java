package mn.ticket.ticket.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name="ticket", schema = "ticketschema")
public class Ticket {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "price_high")
    private Float priceHigh;

    @Column(name = "price_normal")
    private Float priceNormal;

    @Column(name = "deadline_date")
    private Date deadlineDate;

    @Column(name = "payout_method")
    private PayoutMethod payoutMethod;

    @Column(name = "stock")
    private Long stock;

    @OneToOne
    @JoinColumn(name = "event_id", nullable = false)
    private Event event;

}
