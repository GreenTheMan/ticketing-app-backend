package mn.ticket.ticket.model;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name="documentation", schema = "ticketschema")
public class Documentation {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "filename", nullable = false)
    private String filename;

    @Column(name = "is_photo", nullable = false)
    private Boolean isPhoto;

    @ManyToOne
    @JoinColumn(name="event_id")
    private Event event;
}
