package mn.ticket.ticket.dto;

import lombok.*;
import mn.ticket.ticket.model.Event;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class EventDto {

    @NotBlank
    private String name;
    @NotNull
    private Long category_id;

    private LocalDateTime date;
    private String description;
    private Double location_lat;
    private Double location_long;
    private Long ticket_id;

    public EventDto(Event e) {

    }

    public EventDto(Long id, String name) {
        this.name = name;
    }
}