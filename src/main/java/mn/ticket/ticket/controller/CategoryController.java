package mn.ticket.ticket.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mn.ticket.ticket.model.Category;
import mn.ticket.ticket.service.CategoryService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController("category")
@RequestMapping("category")
public class CategoryController {

    private final CategoryService service;

    @GetMapping("findAll")
    public ResponseEntity<List<Category>> findAll(){
        try {
            return ResponseEntity.ok(service.findAll());
        }catch (Exception e){
            log.error(e.getMessage());
            throw e;
        }
    }

    @PostMapping("save")
    public ResponseEntity<Category> save(@RequestBody Category category){
        try {
            return ResponseEntity.ok(service.save(category));
        }catch (Exception e){
            log.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping("findById")
    public ResponseEntity<Category> findById(@RequestParam(value = "id") Long categoryId) {
        try {
            return ResponseEntity.ok(service.findById(categoryId));
        } catch (Exception e) {
            throw e;
        }
    }



}
