package mn.ticket.ticket.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mn.ticket.ticket.dto.EventDto;
import mn.ticket.ticket.model.Category;
import mn.ticket.ticket.model.Event;
import mn.ticket.ticket.service.EventService;
import mn.ticket.ticket.service.HandlerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@RestController("event")
@RequestMapping("event")
public class EventController {

    private final HandlerService service;

    @GetMapping
    public ResponseEntity<List<EventDto>> findAll() {
        try {
            return ResponseEntity.ok(service.findAllEvents());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @PostMapping
    public ResponseEntity<EventDto> save(@RequestBody @Valid EventDto eventDto) {
        try {
            return ResponseEntity.ok(service.saveEvent(eventDto));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping("searchAll")
    public ResponseEntity<List<EventDto>> searchAll(@RequestParam(required = false, defaultValue = "") String name,
                                              @RequestParam(required = false, defaultValue = "") String description,
                                              @RequestParam(required = false, defaultValue = "-1") Long categoryId) {
        try {
            return ResponseEntity.ok(service.searchAllEvents(name, description, categoryId));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

}
