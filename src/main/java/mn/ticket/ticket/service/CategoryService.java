package mn.ticket.ticket.service;

import lombok.RequiredArgsConstructor;
import mn.ticket.ticket.model.Category;
import mn.ticket.ticket.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository repo;

    public Category save(Category category){
        return repo.save(category);
    }

    public Category findById(Long id){
        return repo.findById(id).orElse(null);
    }

    public List<Category> findAll(){
        return repo.findAll();
    }

}
