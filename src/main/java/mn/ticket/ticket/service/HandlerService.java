package mn.ticket.ticket.service;

import lombok.RequiredArgsConstructor;
import mn.ticket.ticket.dto.EventDto;
import mn.ticket.ticket.model.Event;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HandlerService {
    private EventService eventService;
    private CategoryService categoryService;
    private TicketService ticketService;

    public Event eventDtoToEvent(EventDto eventDto) {
        return Event.builder()
                .name(eventDto.getName())
                .category(categoryService.findById(eventDto.getCategory_id()))
                .date(eventDto.getDate())
                .description(eventDto.getDescription())
                .locationLat(eventDto.getLocation_lat())
                .locationLong(eventDto.getLocation_long())
                .ticket(ticketService.findById(eventDto.getTicket_id()))
                .build();
    };

    public List<Event> eventDtoListToEventList(List<EventDto> eventDtoList) {
        return eventDtoList.stream()
                .map(eventDto -> eventDtoToEvent(eventDto))
                .collect(Collectors.toList());
    }

    public EventDto eventToEventDto(Event event) {
        return EventDto.builder()
                .name(event.getName())
                .category_id(event.getCategory().getId())
                .date(event.getDate())
                .description(event.getDescription())
                .location_lat(event.getLocationLat())
                .location_long(event.getLocationLong())
                .ticket_id(event.getTicket().getId())
                .build();
    }

    public List<EventDto> eventListToEventDtoList(List<Event> eventList) {
        return eventList.stream()
                .map(event -> eventToEventDto(event))
                .collect(Collectors.toList());
    }

    public List<EventDto> findAllEvents() {
        return eventListToEventDtoList(eventService.findAll());
    }

    public EventDto saveEvent(EventDto eventDto) {
        return eventToEventDto(eventService.save(eventDtoToEvent(eventDto)));
    }

    public List<EventDto> searchAllEvents(String name, String description, Long categoryId) {
        return eventService.searchDto(name, description, categoryId);
    }

}
