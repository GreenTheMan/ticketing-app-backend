package mn.ticket.ticket.service;

import lombok.RequiredArgsConstructor;
import mn.ticket.ticket.model.Ticket;
import mn.ticket.ticket.repository.TicketRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TicketService {

    private final TicketRepository repo;

    public Ticket findById(Long ticketId) {
        return repo.findById(ticketId).orElse(null);
    }

}
