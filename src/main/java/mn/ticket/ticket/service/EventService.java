package mn.ticket.ticket.service;

import lombok.RequiredArgsConstructor;
import mn.ticket.ticket.dto.EventDto;
import mn.ticket.ticket.model.Event;
import mn.ticket.ticket.repository.CategoryRepository;
import mn.ticket.ticket.repository.EventRepository;
import mn.ticket.ticket.repository.TicketRepository;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository repo;

    public Event save(Event event) {
        return repo.save(event);
    }

    public Event findById(Long eventId) {
        return repo.findById(eventId).orElse(null);
    }

    public List<Event> findAll() {
        return repo.findAll();
    }

    public List<EventDto> searchDto(String name, String description, Long categoryId) {
        return repo.findAllByNameAndDescriptionAndCategory(name, description, categoryId);
    }

//    public List<Event> search(String name, String description, Long categoryId) {
//        return repo.findAllByNameAndDescriptionAndCategory(name, description, categoryId);
//    }

}
